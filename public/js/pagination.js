function Pagination(){
	this.ajax = ajax;
}
Pagination.prototype=
{
	appendPagination : function (nb_pages) 
	{
		var self = this;
		var active = '';
		var links = [];
	  	for (let i = 1; i <= nb_pages; i++) {
	  		active = i == 1 ? 'active' : '';
	  		links.push($("<li>").append(($('<a>',{
				href : '#'+i,
				click : function(){ self.switch_page(i);},
				class : 'page ' + active,
				text : i
			}))));
		};
		$('.pagination').html(links);
	},

	switch_page : function(page_number)
	{
		page_number = page_number >= 1 ? (page_number - 1) * $("#nb_items_page").val() : page_number;
		console.log(page_number);
		this.ajax.openSearch($("#term").val(), page_number);
	}
}
