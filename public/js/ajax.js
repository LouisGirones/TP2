function Ajax(){

}
Ajax.prototype=
{
	openSearch : function (term, start_from) 
	{
		var nb_items = $("#nb_items_page").val();
		$('#result').html("");

		var request = $.ajax({
			url: "http://localhost:1234/api/?action=opensearch&format=json&search=" + term + "&namespace=" + start_from + "&limit=" + nb_items,
			method: "GET",
			dataType: "json"
		});

		request.done(function(result) {
			$("#info").html('');
			$("#info").prepend("<b>Search query : </b>" + result[0] + "<br />" +
				"<b>Result : </b>" + (start_from + 1) + " to " + (start_from + result[1].length) + "<br /><br />");
			var s = "</p>";
			for (i = 0; i < result[1].length; i++) {
				s += template.openSearch(result[1][i], result[2][i], result[3][i])
			}
			s += "</p>";
			$("#result").append(s);

		});

		request.fail(function(jqXHR, textStatus) {
			alert("Request failed: " + textStatus);
		});
	}
}
