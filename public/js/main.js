var ajax = new Ajax(),
	template = new	Template(),
	pagination = new Pagination();


$(document).ready(function() {

	for (i = 10; i <= 50; i += 10) {
		$('#nb_items_page').append($("<option/>", {
    		value : i,
    		text : i
		}));
	}
	
	$("#search").on("submit", function(event) {
		event.preventDefault();
		$('#result').html('');
		$('#page').html('');

		var term = $("#term").val()

		if (term !== "") {
			ajax.openSearch(term, 0);
			pagination.appendPagination(5);
		}

	});

	$(".pagination").on("click",'.page', function(event) {
        $( "ul.pagination li  a.active" ).removeClass( "active" );
        $(this).addClass( "active" );

	});
});



	